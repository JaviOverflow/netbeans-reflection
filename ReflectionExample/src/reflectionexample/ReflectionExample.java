/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reflectionexample;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

class Person {
    public Person() { }
    public Person(String name, int age) { }

    public int getAge() { return 10; }

    public void sayThis(String message) {
        System.out.printf("%s", message);
    }
}

public class ReflectionExample {

    void init() throws IllegalAccessException, InstantiationException {
        Person personObject = new Person();
        checkThisObject(personObject);
    }
    
    void checkThisObject(Object anObject) throws InstantiationException, IllegalAccessException {
        
        // To obtain the class of the object
        Class personClass = anObject.getClass();
        
        // We can get the name of the class
        String className = personClass.getName();
        System.out.printf("The class of the object is: %s\n", className);
        
        // From the class we can instantiate other objects
        //Person anotherPerson = (Person) personClass.newInstance();
        
        // Or we can check what constructors the class has
        Constructor[] constructors = personClass.getConstructors();
        
        // How many constructors?
        int constructorsAmount = constructors.length;
        System.out.printf("The class has %d constructors\n", constructorsAmount);
        
        // And print information about each constructor
        for (Constructor constructor : constructors) {
            Class[] parametersClasses = constructor.getParameterTypes();
            int parametersAmount = parametersClasses.length;
            System.out.printf("  Constructor with %d parameters\n", parametersAmount);
            
            for (Class parameterClass : parametersClasses) {
                System.out.printf("    Parameter class %s\n", parameterClass.getName());
            }
        }
        
        // Or check information about methods
        Method[] methods = personClass.getMethods();
        
        // How many methods?
        int methodsAmount = methods.length;
        System.out.printf("The class has %d methods\n", methodsAmount);
        
        // Or check information about its methods
        for (Method method : methods) {
            
            String methodName = method.getName();
            System.out.printf("Method called %s ", methodName);

            Class returnClass = method.getReturnType();
            System.out.printf("that returns %s ", returnClass.getName());
            
            Class[] parametersClasses = method.getParameterTypes();
            System.out.printf("and has %d parameters\n", parametersClasses.length);

            for (Class parameterClass : parametersClasses) {
                System.out.printf("    Parameter class %s\n", parameterClass.getName());
            }        
        }
        
        
    }
    
    public static void main(String[] args) throws IllegalAccessException, InstantiationException {
        (new ReflectionExample()).init();
    }
}
